<html>
<head>
    <title>API | Task 4</title>
</head>
<body>

    <?php
        class API {
            public function printFullName($fullName) {
                echo "<strong>Full Name:</strong> " . $fullName . "<br>";
            }
            public function printHobbies($hobbies) {
                echo "<strong>Hobbies:</strong><br>";
                foreach ($hobbies as $hobby) {
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $hobby . "<br>";
                }
            }
            public function printPersonalInfo($personalInfo) {
                echo "<strong>Age:</strong> {$personalInfo->age}<br>";
                echo "<strong>Email:</strong> {$personalInfo->email}<br>";
                echo "<strong>Birthday:</strong> {$personalInfo->birthday}<br>";
            }
        }
        $api = new API();

        //print fullname
        $fullName = "Jake Dajao Prahinog";
        $api->printFullName($fullName);

        //print hobbies
        $hobbies = ["Coding", "Playing the Guitar", "Basketball", "Video Games", "Driving"];
        $api->printHobbies($hobbies);

        //print personal info
        $personalInfo = (object) [
            'age' => 22,
            'email' => 'prahinogjake@gmail.com',
            'birthday' => 'October 06, 2001'
        ];
        $api->printPersonalInfo($personalInfo);
    ?>

</body>
</html>

